import time

import serial

from log_writer import LogWriter
from usart_control_interface import USARTControlInterface

device_file = "COM3"

if __name__ == '__main__':
    # setting
    ser = serial.Serial(device_file)
    ser.baudrate = 115200
    ser.timeout = 1
    ser.bytesize = serial.EIGHTBITS
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE

    interface = USARTControlInterface(ser)
    interface.print_available_grasps()
    writer = LogWriter(interface, "grasp_strength_2", 10)
    writer.start()
    while True:
        time.sleep(10)

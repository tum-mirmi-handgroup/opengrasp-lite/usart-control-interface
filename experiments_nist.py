import time

import serial

from log_writer import LogWriter
from usart_control_interface import USARTControlInterface, FingerInput, GraspInput

device_file = "COM3"

experiment = "finger_strength"  # Must be one of [finger_repeatability, touch_sensitivity, finger_strength, grasp_strength]
finger = "thumb"  # Must be one of [thumb, index, middle, ring, pinky]


def _nist_get_finger_index():
    global finger
    if finger == "thumb":
        return 0
    elif finger == "index":
        return 1
    elif finger == "middle":
        return 2
    elif finger == "ring":
        return 3
    elif finger == "pinky":
        return 4
    else:
        raise ValueError


def nist_finger_repeatability(interface: USARTControlInterface):
    num_loops = 50
    finger_close = FingerInput(
        finger_number=_nist_get_finger_index(),
        position=0.5,
        velocity=1000,
        current=1.0,
        do_position_ctrl=True,
        do_velocity_ctrl=True,
        stop_at_contact=False,
        use_aggressive_contact=False,
    )
    finger_open = FingerInput(
        finger_number=_nist_get_finger_index(),
        position=0,
        velocity=1000,
        current=1.0,
        do_position_ctrl=True,
        do_velocity_ctrl=True,
        stop_at_contact=False,
        use_aggressive_contact=False,
    )
    writer = LogWriter(interface,
                       "result_{}_nist_finger_repeatability_{}".format(time.strftime("%Y%m%d-%H%M%S"), finger), 10)
    writer.start()
    interface.send_stop()
    interface.send_finger_control(finger_open)
    time.sleep(2)

    for i in range(num_loops):
        print("Running {} / {}".format(i + 1, num_loops))
        interface.send_finger_control(finger_close)
        time.sleep(3)
        interface.send_finger_control(finger_open)
        time.sleep(2)
    writer.stop()
    time.sleep(2)


def nist_touch_sensitivity(interface: USARTControlInterface):
    num_loops = 50
    finger_close = FingerInput(
        finger_number=_nist_get_finger_index(),
        position=1,
        velocity=1000,
        current=0.2,
        do_position_ctrl=True,
        do_velocity_ctrl=True,
        stop_at_contact=True,
        use_aggressive_contact=False,
    )
    finger_open = FingerInput(
        finger_number=_nist_get_finger_index(),
        position=0,
        velocity=1000,
        current=0.5,
        do_position_ctrl=True,
        do_velocity_ctrl=True,
        stop_at_contact=False,
        use_aggressive_contact=False,
    )
    writer = LogWriter(interface, "result_{}_nist_touch_sensitivity_{}".format(time.strftime("%Y%m%d-%H%M%S"), finger),
                       10)
    writer.start()
    interface.send_stop()
    interface.send_finger_control(finger_open)
    time.sleep(2)

    for i in range(num_loops):
        print("Running {} / {}".format(i + 1, num_loops))
        interface.send_finger_control(finger_close)
        time.sleep(3)
        interface.send_finger_control(finger_open)
        time.sleep(2)
    writer.stop()
    time.sleep(2)


def nist_finger_strength(interface: USARTControlInterface):
    num_loops = 50
    finger_close = FingerInput(
        finger_number=_nist_get_finger_index(),
        position=1,
        velocity=1000,
        current=1.0,
        do_position_ctrl=True,
        do_velocity_ctrl=True,
        stop_at_contact=False,
        use_aggressive_contact=False,
    )
    finger_open = FingerInput(
        finger_number=_nist_get_finger_index(),
        position=0,
        velocity=1000,
        current=1.0,
        do_position_ctrl=True,
        do_velocity_ctrl=True,
        stop_at_contact=False,
        use_aggressive_contact=False,
    )
    writer = LogWriter(interface, "result_{}_nist_finger_strength_{}".format(time.strftime("%Y%m%d-%H%M%S"), finger),
                       10)
    writer.start()
    interface.send_stop()
    interface.send_finger_control(finger_open)
    time.sleep(2)

    for i in range(num_loops):
        print("Running {} / {}".format(i + 1, num_loops))
        interface.send_finger_control(finger_close)
        time.sleep(2)
        interface.send_stop()
        time.sleep(1)
        interface.send_finger_control(finger_open)
        time.sleep(7)
    writer.stop()
    time.sleep(2)


def nist_grasp_strength(interface: USARTControlInterface):
    num_loops = 10
    grasp_close = GraspInput(
        grasp_number=13,
        speed=50,
        power=100,
        auto_continue=True
    )
    fingers_open = [
        FingerInput(
            finger_number=i,
            position=0.3,
            velocity=1000,
            current=1.0,
            do_position_ctrl=True,
            do_velocity_ctrl=True,
            stop_at_contact=False,
            use_aggressive_contact=False,
        ) for i in range(6)
    ]
    writer = LogWriter(interface, "result_{}_nist_grasp_strength_{}".format(time.strftime("%Y%m%d-%H%M%S"), finger), 10)
    writer.start()
    interface.send_stop()
    for finger_open in fingers_open:
        interface.send_finger_control(finger_open)
    time.sleep(2)

    for i in range(num_loops):
        print("Running {} / {}".format(i + 1, num_loops))
        interface.send_grasp_control(grasp_close)
        time.sleep(5)
        interface.send_stop()
        time.sleep(10)
        for finger_open in fingers_open:
            interface.send_finger_control(finger_open)
        time.sleep(5)
        time.sleep(15)
    writer.stop()
    time.sleep(2)


if __name__ == '__main__':
    # setting
    ser = serial.Serial(device_file)
    ser.baudrate = 230400
    ser.timeout = 1
    ser.bytesize = serial.EIGHTBITS
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    inter = USARTControlInterface(ser)
    if experiment == "finger_repeatability":
        nist_finger_repeatability(inter)
    elif experiment == "touch_sensitivity":
        nist_touch_sensitivity(inter)
    elif experiment == "finger_strength":
        nist_finger_strength(inter)
    elif experiment == "grasp_strength":
        nist_grasp_strength(inter)

    print("Success!")

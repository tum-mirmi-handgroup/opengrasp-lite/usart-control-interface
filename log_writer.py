import csv
import time
from threading import Thread, Event
from typing import Optional

from usart_control_interface import USARTControlInterface


class LogWriter:
    interface: USARTControlInterface
    thread: Optional[Thread]
    run_flag: Event
    log_file: str
    start_time: float
    next_time: float
    last_print_time: float
    delay: float

    def __init__(self, interface: USARTControlInterface, file_base_name: str, frequency: float):
        self.interface = interface
        self.thread = None
        self.run_flag = Event()
        self.log_file = file_base_name
        self.delay = 1 / frequency

    def start(self):
        if self.thread is not None:
            return
        self.run_flag.set()
        self.last_print_time = float("-inf")
        self.next_time = time.monotonic()
        self.start_time = self.next_time
        self.thread = Thread(target=LogWriter._run, args=(self,), daemon=True)
        self.thread.start()
        time.sleep(1)

    def _run(self):
        with open(self.log_file + "_info.txt", "w") as info_file:
            info_file.write("Gear Ratios\n")
            ratios = self.interface.read_gear_ratio()
            info_file.write(", ".join([str(ratio) for ratio in ratios] if ratios is not None else []))

        with (open(self.log_file + ".csv", "w", newline="") as csv_file):
            writer = csv.writer(csv_file)
            header = [
                "time",
                *[f"tactile_{i}" for i in range(5)],
                *[f"pos_{i}" for i in range(6)],
                *[f"vel_{i}" for i in range(6)],
                *[f"cur_{i}" for i in range(6)],
                *[f"vol_{i}" for i in range(6)],
                "\n",
            ]
            csv_file.write(", ".join(header))

            while self.run_flag.is_set():
                time.sleep(max(0.0, self.next_time - time.monotonic()))

                current_time = self.next_time - self.start_time

                tactile = self.interface.read_tactile()
                if tactile is None:
                    print("Failed Tactile transmission")
                else:
                    tactile = (tactile[8], tactile[2], tactile[0], tactile[4], tactile[6])

                pos = self.interface.read_position()
                if pos is None:
                    print("Failed Position transmission")

                vel = self.interface.read_velocity()
                if vel is None:
                    print("Failed Velocity transmission")

                cur = self.interface.read_current()
                if cur is None:
                    print("Failed Current transmission")

                vol = self.interface.read_voltage()
                if vol is None:
                    print("Failed Voltage transmission")

                if tactile is not None and pos is not None and vel is not None and cur is not None:
                    data = tuple([current_time]) + tactile + pos + vel + cur + vol
                    assert len(data) == len(header)
                    writer.writerow(data)
                    if current_time - self.last_print_time >= 4.95:
                        self.last_print_time = current_time
                        print(f"{current_time:>5.1f}: Writing logs...")

                if (time.monotonic() - self.next_time) // self.delay > 0:
                    print(f"Skipping {(time.monotonic() - self.next_time) // self.delay}  ...")
                self.next_time += (time.monotonic() - self.next_time) // self.delay * self.delay + self.delay

    def stop(self):
        self.run_flag.clear()
        self.thread.join()
        self.thread = None


if __name__ == "__main__":
    class InterfaceMock:
        def __init__(self):
            self.timestamp = 0

        def read_gear_ratio(self):
            return tuple([i for i in range(6)])

        def read_tactile(self):
            print("Reading")
            self.timestamp += 1
            offset = self.timestamp + 0.1
            time.sleep(0.05)
            return tuple([offset + i for i in range(16)])

        def read_position(self):
            offset = self.timestamp + 0.01
            time.sleep(0.05)
            return tuple([offset + i for i in range(6)])

        def read_velocity(self):
            offset = self.timestamp + 0.001
            time.sleep(0.05)
            return tuple([offset + i for i in range(6)])

        def read_current(self):
            offset = self.timestamp + 0.0001
            time.sleep(0.05)
            return tuple([offset + i for i in range(6)])

        def read_temperature(self):
            offset = self.timestamp + 0.00001
            time.sleep(0.05)
            return tuple([offset + i for i in range(6)])


    lw = LogWriter(InterfaceMock(), "test", 1.0)
    lw.start()
    time.sleep(10.1)
    lw.stop()

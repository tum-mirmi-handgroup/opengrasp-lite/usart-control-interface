import struct
import time
from dataclasses import dataclass, field
from enum import IntEnum
from threading import Lock
from typing import Optional, Tuple, List

import serial


def _pack(fmt: str, *args) -> bytes:
    return struct.pack("<" + fmt, *args)


def _unpack(fmt: str, data: bytes):
    if len(data) < struct.calcsize(fmt):
        return None
    return struct.unpack("<" + fmt, data)


class Requests(IntEnum):
    """
    Request codes the main controller will answer to
    """
    STATE = 0xE0
    NUM_GRASPS = 0xE1
    GRASP_NAME = 0xE2
    TACTILE = 0xE3
    GEAR_RATIO = 0xE4
    POSITION = 0xE5
    VELOCITY = 0xE6
    CURRENT = 0xE7
    VOLTAGE = 0xE8
    TEMPERATURE = 0xE9
    GRASP = 0xF0
    FINGER = 0xF1
    STOP = 0xFF


@dataclass(frozen=True)
class State:
    """
    This will represent the state of the main controller.
    As of now this feature is not implemented and the main controller will only return 20 bytes of zeros
    """
    raw: bytes


@dataclass(frozen=True)
class GraspInput:
    """
    This represents a grasp

    :param grasp_number: The number of the grasp, must be within 0-255. Use either
    USARTControlInterface.print_available_grasps or the serial console of the main controller to see all available
    :param speed: The speed of the grasp, must be within 0-100
    :param power: The power the grasp will use as soon as all fingers have contact, must be within 0-100
    :param auto_continue: Some Grasps have a following grasp defined if true the following grasp will be executed
    automatically
    """
    grasp_number: int
    speed: int = field(default=25)
    power: int = field(default=25)
    auto_continue: bool = field(default=True)

    def serialize(self) -> bytes:
        return _pack("BBBB", self.grasp_number, self.speed, self.power, 1 if self.auto_continue else 0)


@dataclass(frozen=True)
class FingerInput:
    """
    This represents input to a single finger

    :param finger_number: The number of the finger, 0 thumb, 1 index, 2 middle, 3 ring, 4 pinky, 5 misc
    """
    finger_number: int
    position: float
    velocity: float = field(default=0.6)
    current: float = field(default=0.3)
    do_position_ctrl: bool = field(default=True)
    do_velocity_ctrl: bool = field(default=True)
    stop_at_contact: bool = field(default=True)
    use_aggressive_contact: bool = field(default=True)

    def serialize(self) -> bytes:
        flags = 0
        if not self.do_position_ctrl:
            if self.do_velocity_ctrl:
                flags |= 0b0000_0001
            else:
                flags |= 0b0000_0010
        # By setting 0b0000_0100 high you can deactivate stopping at the movement range. This is pretty dangerous, so
        # we decided to not make it accessible as easily
        if self.stop_at_contact:
            flags |= 0b0000_1000
        if self.use_aggressive_contact:
            flags |= 0b0001_0000
        return _pack("BfffB", self.finger_number, self.position, self.velocity, self.current, flags)


class USARTControlInterface:
    serial_hw: serial.Serial
    available_grasps: List[str]
    lock: Lock

    @dataclass(frozen=True)
    class _Package:
        interface: "USARTControlInterface"
        command: Optional[Requests]
        payload: Optional[bytes] = field(default=None, init=True)

        def transmit(self, hw: serial.Serial) -> "USARTControlInterface._Package":
            self.interface.lock.acquire()
            if self.payload is None:
                header = _pack("BB", self.command, 0)
                hw.write(header)
            else:
                hw.write(_pack("B",self.command))
                time.sleep(0.00002)
                hw.write(_pack("B", len(self.payload)))
                time.sleep(0.00002)
                for byte in self.payload:
                    hw.write(byte.to_bytes(1, byteorder="little"))
                    time.sleep(0.00002)
            ret = USARTControlInterface._Package._receive(self.interface, self.command, hw)
            self.interface.lock.release()
            return ret

        @staticmethod
        def _receive(interface: "USARTControlInterface", tx_command: Requests,
                     hw: serial.Serial) -> "USARTControlInterface._Package":
            data = _unpack("BB", hw.read(2))
            if data is None:
                hw.flush()
                return USARTControlInterface._Package(interface, None, None)
            command, payload_length = data
            command = Requests(command | 0x80)
            assert command == tx_command, f"Received {command.name}, expected {tx_command.name}"
            if payload_length > 0:
                payload = hw.read(payload_length)
                return USARTControlInterface._Package(interface, command, payload)
            else:
                return USARTControlInterface._Package(interface, command, None)

        def payload_as_floats(self) -> Optional[Tuple[float, ...]]:
            if self.payload is None:
                return None
            assert len(self.payload) % 4 == 0, "Data length must be a multiple of 4"
            return _unpack("f" * (len(self.payload) // 4), self.payload)

        def payload_as_uint8_t(self) -> Optional[int]:
            if self.payload is None:
                return None
            assert len(self.payload) == 1, "Data length must be 20"
            return _unpack("B", self.payload)[0]

        def payload_as_string(self) -> Optional[str]:
            if self.payload is None:
                return None
            return self.payload.decode("utf-8")

        def payload_as_state(self) -> Optional[State]:
            if self.payload is None:
                return None
            assert len(self.payload) == 20, "Data length must be 20"
            return State(self.payload[:])

    def __init__(self, serial_hw: serial.Serial):
        self.serial_hw = serial_hw
        self.serial_hw.flush()
        self.send_reset()
        self.available_grasps = []
        self.lock = Lock()

        num_grasps = self._Package(self, Requests.NUM_GRASPS).transmit(self.serial_hw).payload_as_uint8_t()
        if num_grasps is not None and num_grasps > 0:
            for i in range(num_grasps):
                self.available_grasps.append(
                    self._Package(self, Requests.GRASP_NAME, _pack("B", i)).transmit(
                        self.serial_hw).payload_as_string())

        self.send_reset()

    def send_reset(self):
        self.serial_hw.flush()
        self.serial_hw.write(b"\xff")
        time.sleep(0.05)
        self.serial_hw.write(b"\xff")
        time.sleep(0.05)
        self.serial_hw.write(b"\x00")
        time.sleep(0.05)
        self.serial_hw.write(b"\xfe")
        time.sleep(0.05)
        self.serial_hw.write(b"\x01")
        time.sleep(0.05)
        self.serial_hw.write(b"\xfd")
        time.sleep(0.05)
        self.serial_hw.write(b"\x02")
        time.sleep(0.05)
        self.serial_hw.write(b"\xfc")
        time.sleep(0.05)
        self.serial_hw.write(b"\x03")
        time.sleep(0.05)

    def print_available_grasps(self) -> None:
        """ Print all available grasps, that can be used with the GraspInput """
        print("Available Grasps:")
        for i, grasp in enumerate(self.available_grasps):
            print(f"{i:>3d}: {grasp}")

    def read_state(self) -> Optional[State]:
        """
        Get the state of the main controller

        This is currently not implemented and will only receive 20 bytes of zeros
        """
        return self._Package(self, Requests.STATE).transmit(self.serial_hw).payload_as_state()

    def read_tactile(self, sensor: int = 0xFF) -> Optional[Tuple[float, ...]]:
        """
        Get the tactile sensor data

        :param int sensor: set this to get only a single sensor. If the wanted sensor is not available, it returns all
        :return: None on transmission error
        """
        return self._Package(self, Requests.TACTILE, _pack("B", sensor)).transmit(self.serial_hw).payload_as_floats()

    def read_gear_ratio(self, finger: int = 0xFF) -> Optional[Tuple[float]]:
        """
        Get the gear ratio of the motor (without the worm gear)

        :param int finger: set this to get only a single finger. If the wanted finger is not available, it returns all
        :return: None on transmission error
        """
        return self._Package(self, Requests.GEAR_RATIO, _pack("B", finger)).transmit(self.serial_hw).payload_as_floats()

    def read_position(self, finger: int = 0xFF) -> Optional[Tuple[float]]:
        """
        Get the position motor, within 0 to 1 - 0 is fully open, 1 is closed

        :param int finger: set this to get only a single finger. If the wanted finger is not available, it returns all
        :return: None on transmission error
        """
        return self._Package(self, Requests.POSITION, _pack("B", finger)).transmit(self.serial_hw).payload_as_floats()

    def read_velocity(self, finger: int = 0xFF) -> Optional[Tuple[float]]:
        """
        Get the velocity of the motor in [rad/s]

        :param int finger: set this to get only a single finger. If the wanted finger is not available, it returns all
        :return: None on transmission error
        """
        return self._Package(self, Requests.VELOCITY, _pack("B", finger)).transmit(self.serial_hw).payload_as_floats()

    def read_current(self, finger: int = 0xFF) -> Optional[Tuple[float]]:
        """
        Get the current of the motor in [Ampere]

        :param int finger: set this to get only a single finger. If the wanted finger is not available, it returns all
        :return: None on transmission error
        """
        return self._Package(self, Requests.CURRENT, _pack("B", finger)).transmit(self.serial_hw).payload_as_floats()

    def read_voltage(self, finger: int = 0xFF) -> Optional[Tuple[float]]:
        """
        Get the voltage of the motor in [Volt]

        :param int finger: set this to get only a single finger. If the wanted finger is not available, it returns all
        :return: None on transmission error
        """
        return self._Package(self, Requests.VOLTAGE, _pack("B", finger)).transmit(self.serial_hw).payload_as_floats()

    def read_temperature(self, finger: int = 0xFF) -> Optional[Tuple[float]]:
        """
        Get the guessed temperature above ambient of the motor in [Kelvin]

        :param int finger: set this to get only a single finger. If the wanted finger is not available, it returns all
        :return: None on transmission error
        """
        return self._Package(self, Requests.TEMPERATURE, _pack("B", finger)).transmit(
            self.serial_hw).payload_as_floats()

    def send_grasp_control(self, grasp: GraspInput) -> bool:
        """
        Execute a grasp with the hand

        :return: False on transmission error
        """
        return self._Package(self, Requests.GRASP, grasp.serialize()).transmit(self.serial_hw) is not None

    def send_finger_control(self, finger: FingerInput) -> bool:
        """
        Directly control a single finger of the hand

        :return: False on transmission error
        """
        return self._Package(self, Requests.FINGER, finger.serialize()).transmit(self.serial_hw) is not None

    def send_stop(self) -> bool:
        """
        Stops all fingers, even if the main controller does not listen to the USART control inputs

        :return: False on transmission error
        """
        return self._Package(self, Requests.STOP).transmit(self.serial_hw) is not None

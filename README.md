# USART Control Interface

The USART Control Interface is a python project, that allows for programatic access to the main controller software via USART, similarly to the serial console that is used with a program like Putty. The USART Control Interface is not human readable as it uses raw data and it only consists of a subset of the functionallity that the serial console offers.
